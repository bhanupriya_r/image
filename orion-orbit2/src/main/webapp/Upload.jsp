<html ng-app="fileUpload">
     <head >

      <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/5.0.9/ng-file-upload-shim.js">
     </script>
         <link rel="stylesheet" type="text/css" href="resources/common/css/upload.css">




        <script src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/5.0.9/ng-file-upload.js"></script>
          <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
         <script src="resources/js/upload.js"></script>


    </head>

<body  ng-controller="MyCtrl">

<div >


    <div class="button" ngf-select ng-model="files" ngf-multiple="multiple">Select File</div>

    Drop File:
    <div ngf-drop ngf-select ng-model="files" class="drop-box"
        ngf-drag-over-class="dragover" ngf-multiple="true" ngf-allow-dir="true"
        accept="image/*,application/pdf">Drop pdfs or images here or click to upload</div>
    <div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
    Files:

    <ul>
        <li ng-repeat="f in filesDetails" style="font:smaller">{{f.name}}</li>
    </ul>

</div>
</div>

<div>
         <button class="" ng-click="uploadImage()">Upload Image</button>
</div>


</body>

</html>