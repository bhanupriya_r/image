var app = angular.module('newClue', ['ngTagsInput', 'ui.bootstrap']);
app.controller(
  'NewClueController', ['$scope', '$http', 'countryListFactory', 'cityListService', 'placelistService', 'tagService', function($scope, $http, countryListFactory, cityListService, placelistService, tagService) {

    $scope.tagsValues = '';

    countryListFactory.getCountry()
      .success(function(data) {
        $scope.countries = data
      })
      .error(

        function(data, status, headers,
          config) {

        });

    $scope.getCityNames = function() {

      cityListService.getCity($scope.Countryselected.cntryCode)
        .success(function(data) {
          $scope.getCities = data;

        })
        .error(

          function(data, status, headers,
            config) {

          });

    }

    $scope.getPlace = function() {

      $scope.availablePlaces = [];
      placelistService.getplaceList($scope.cityselect.cityCode)
        .success(function(data) {
          $scope.getPlaces = data;

        })
        .error(

          function(data, status, headers,
            config) {

          });

      angular.forEach(
        $scope.getPlaces,

        function(value) {
          if (value.getPlaces != ' ') {
            $scope.availablePlaces.push(value);
          }

        });

      sessionStorage.setItem('answers', JSON.stringify($scope.availablePlaces));
      $scope.items = JSON.parse(sessionStorage.getItem('answers'));

    };

    $scope.loadTags = function($query)

    {
      return tagService.getTags()
        .then(function(response) {
          var countries = response.data;
          return countries.filter(function(country) {
            return country.tag.toLowerCase()
              .indexOf($query.toLowerCase()) != -1;
          });
        });
    };

    $scope.getTags = function() {

      $scope.tagsValues = $scope.tags2.map(function(tag) {
        return tag.tagId;
      });

    };

    $scope.submit = function() {

      var formData = {
        clue: $scope.selectedclue,
        clueLvl: $scope.selected_rating,
        clueDesc: $scope.cluedescription,

        ansId: $scope.placeselected.ansId,
        tagIds: $scope.tagsValues

      };

      var newcluepost = $http.post(
        'http://localhost:8080/orion-orbit/newclue/saveAndSubmit',
        JSON.stringify(formData));

      newcluepost.success(function(data, status,
        headers, config) {

      });
      newcluepost.error(function(data, status,
        headers, config) {
        alert("Exception details: " + JSON.stringify({
          data: data
        }));

      });

      $scope.Countryselected = '';
      $scope.cityselect = '';
      $scope.placeselected = '';
      $scope.selectedclue = '';
      $scope.selected_rating = '';
      $scope.cluedescription = '';
      $scope.tags2 = '';
    };

  }]);