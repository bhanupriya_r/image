app.factory('countryListFactory', [
  '$http',
  function($http) {

    var url;
    return {
      getCountry: function() {
        url = 'http://localhost:8080/orion-orbit/newclue/country/list';
        return $http.get(url);
      }
    }
  }
]);