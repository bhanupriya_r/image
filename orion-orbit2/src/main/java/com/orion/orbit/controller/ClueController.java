package com.orion.orbit.controller;


import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.loader.custom.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.orion.orbit.dao.ClueDao;
import com.orion.orbit.exception.ClueNotFoundException;
import com.orion.orbit.model.UploadFile;
import com.orion.orbit.services.ClueServices;

@Controller
@RequestMapping("/newclue")
public class ClueController {

	@Autowired
	ClueServices clueServices;
	
	@Autowired
	ClueDao clueDAO;
	
	static final Logger logger = Logger.getLogger(ClueController.class);
	
	@ExceptionHandler(ClueNotFoundException.class)
	public ModelAndView handleException(HttpServletRequest request, Exception ex){
		logger.error("Requested URL="+request.getRequestURL());
		logger.error("Exception Raised="+ex);
		
		ModelAndView modelAndView = new ModelAndView();
	    modelAndView.addObject("exception", ex);
	    modelAndView.addObject("url", request.getRequestURL());
	    
	    modelAndView.setViewName("error");	    
	    return modelAndView;
	}
		
@RequestMapping(value = "/upload", method = RequestMethod.GET)
public String showUploadForm(HttpServletRequest request) {
    return "Upload";
}
@RequestMapping(value="/doUpload", method = RequestMethod.POST )
public @ResponseBody String  handleFileUpload(HttpServletRequest request,		
@RequestParam CommonsMultipartFile[] fileUpload) throws Exception {
	try {
		if(fileUpload !=null && fileUpload.length>0){
			for(CommonsMultipartFile aFile: fileUpload){
				System.out.println("Saving file: " + aFile.getOriginalFilename());
                UploadFile uploadFile = new UploadFile();
                uploadFile.setData(aFile.getBytes());
                System.out.println("before");
                clueServices.save(uploadFile); 
                System.out.println("after");
			}
		}
	}
	catch (Exception e) {
	throw new ClueNotFoundException(e);
	}
	return "Success";
}
}




	


