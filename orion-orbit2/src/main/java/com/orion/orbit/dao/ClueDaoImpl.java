package com.orion.orbit.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;


import com.orion.orbit.model.UploadFile;

public class ClueDaoImpl implements ClueDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;


	@Transactional
	public void save(UploadFile uploadFile) throws Exception {
		
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(uploadFile);	
		tx.commit();
		session.close();
	}


	
}
