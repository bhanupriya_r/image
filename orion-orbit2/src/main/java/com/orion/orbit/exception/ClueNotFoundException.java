package com.orion.orbit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Employee Not Found") //404
public class ClueNotFoundException extends Exception {


	public ClueNotFoundException(Exception e){
		super("ClueNotFoundException ",e);
	}
}
